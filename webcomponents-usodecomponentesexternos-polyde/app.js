import '@dile/dile-nav/dile-nav.js';
import '@dile/dile-menu-hamburger/dile-menu-hamburger.js';

import '@dile/dile-selector/dile-selector.js';
import '@dile/dile-selector/dile-selector-item.js';

//Define varias páginas y decirle que página quieres que se muestre
import '@dile/dile-pages/dile-pages.js';


//Al seleccionar alguna página quitar el menu en automático
document.getElementById('mainselector').addEventListener( 'dile-selected-changed' , function() {
    document.querySelector('dile-menu-hamburger').close();
});
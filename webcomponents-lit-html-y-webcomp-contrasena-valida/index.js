/**Creación de proyecto con dependencias: lit-html y polymer-cli, creación de tarea para
ejecutar el servidor de polymer
**/

//Importación de las funciones html y render
import { html, render } from 'lit-html';

//Creación de componente web, mediante una clase que extiende de HTMLElement
class PasswordChecker extends HTMLElement {
  //Constructor para inicializar variables y crear shadow DOM del componente
  constructor() {
    super();
    this.attachShadow({mode: 'open'});
    //Propiedad para guardar la contraseña y se inicializa con un valor definido por el usuario
    //Se obtiene el valor de la contraseña
    this.password = this.getAttribute('password');
  }

  get password() { return this._password; }

  //Forzar la actualización del componente
  set password(value) {
    this._password = value;
    this.setAttribute('password', value);
    this.update();
  }

  //Invoca una función render para actualizar el UI del componente
  update() {
    render(this.template(), this.shadowRoot, {eventContext: this});
  }

  //Función para válidar la contraseña
  isValid(passwd) {
    const re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,}/;
    return re.test(passwd);
  }

  //Definición de la representación visual del usuario UI, definir plantilla html, se verifica si es válida
  //o no la contraseña para mostrar o no la barra de progreso y su html correspondiente
  template() {
    return html`
      <span>Your password is <strong>${this.isValid(this.password) ? 'valid 👍' : 'INVALID 👎'}</strong></span>
      ${this.isValid(this.password) ?
        html`<div>Strength: <progress value=${this.password.length-3} max="5"</progress></div>` : ``}`;
  }
}

//Registrar el componente para poder utilizarlo desde nuestr html
customElements.define('password-checker', PasswordChecker);